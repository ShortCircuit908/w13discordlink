package dev.shortcircuit908.w13discordlink;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

public class ConnectedCache {
	private final W13DiscordLink plugin;
	
	public ConnectedCache(W13DiscordLink plugin) {
		this.plugin = plugin;
	}
	
	public void cacheConnectedUser(UUID uuid, String username, long last_joined) throws SQLException {
		try (Connection connection = plugin.getConnectedCacheDataSource().getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "INSERT OR REPLACE INTO `connected_cache` (`player_id`, `username`, `last_joined`) VALUES (?, " +
							 "?, ?)")) {
			statement.setString(1, Utils.uuidToString(uuid));
			statement.setString(2, username);
			statement.setLong(3, last_joined);
			statement.execute();
		}
	}
	
	public Optional<UUID> getCachedUser(String username) throws SQLException {
		try (Connection connection = plugin.getConnectedCacheDataSource().getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT `player_id` FROM `connected_cache` WHERE `username`=? ORDER BY `last_joined` DESC LIMIT" +
							 " 1")) {
			statement.setString(1, username);
			try (ResultSet result = statement.executeQuery()) {
				if (result.next()) {
					return Optional.of(Utils.stringToUuid(result.getString(1)));
				}
			}
		}
		return Optional.empty();
	}
}
