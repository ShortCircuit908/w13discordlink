package dev.shortcircuit908.w13discordlink.event;

import java.util.HashSet;
import java.util.Set;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;
import org.jetbrains.annotations.NotNull;

public class DiscordJoinEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	private final PlayerJoinEvent parent;
	private final String user_snowflake;
	
	public DiscordJoinEvent(PlayerJoinEvent parent, String user_snowflake) {
		this.parent = parent;
		this.user_snowflake = user_snowflake;
	}
	
	public PlayerJoinEvent getParent() {
		return parent;
	}
	
	public String getUserSnowflake() {
		return user_snowflake;
	}
	
	@Override
	public @NotNull HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList(){
		return handlers;
	}
}
