package dev.shortcircuit908.w13discordlink.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.jetbrains.annotations.NotNull;

public class AsyncDiscordPreLoginEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	private final AsyncPlayerPreLoginEvent parent;
	private final String user_snowflake;
	
	public AsyncDiscordPreLoginEvent(AsyncPlayerPreLoginEvent parent, String user_snowflake) {
		super(true);
		this.parent = parent;
		this.user_snowflake = user_snowflake;
	}
	
	public AsyncPlayerPreLoginEvent getParent() {
		return parent;
	}
	
	public String getUserSnowflake() {
		return user_snowflake;
	}
	
	@Override
	public @NotNull HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
