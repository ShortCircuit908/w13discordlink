package dev.shortcircuit908.w13discordlink;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;
import javax.security.auth.login.LoginException;
import javax.sql.DataSource;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.plugin.ApiVersion;
import org.bukkit.plugin.java.annotation.plugin.Description;
import org.bukkit.plugin.java.annotation.plugin.LogPrefix;
import org.bukkit.plugin.java.annotation.plugin.Plugin;
import org.bukkit.plugin.java.annotation.plugin.author.Author;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteDataSource;

@Plugin(name = "w13discordlink", version = "1.1.1")
@Author("ShortCircuit908")
@ApiVersion(ApiVersion.Target.v1_15)
@Description("Platform to link Minecraft and Discord accounts")
@LogPrefix("W13DL")
public class W13DiscordLink extends JavaPlugin {
	private final DiscordMinecraftLink account_links = new DiscordMinecraftLink(this);
	private final DiscordBot bot = new DiscordBot(this);
	private DataSource data_source;
	private DataSource connected_cache_data_source;
	private final Config config = new Config();
	private final ConnectedCache connected_cache = new ConnectedCache(this);
	
	@Override
	public void onEnable() {
		saveDefaultConfig();
		reloadConfig();
		
		initializeSQLiteDatabase();
		getServer().getPluginManager().registerEvents(new LinkListener(this), this);
		
		boolean noop = false;
		
		if (getCustomConfig().getBotToken() == null || getCustomConfig().getBotToken().trim().isEmpty()) {
			getLogger().severe("Discord bot token has not been set");
			noop = true;
		}
		
		if (!noop) {
			try {
				bot.start();
			}
			catch (LoginException | InterruptedException e) {
				getLogger().severe("Error starting Discord bot: " + e.getClass().getName() + ": " + e.getMessage());
			}
		}
		
		if (noop) {
			getLogger().severe("Discord accounts will not be linked");
		}
	}
	
	@Override
	public void onDisable() {
		bot.stop();
	}
	
	@Override
	public void onLoad() {
	
	}
	
	public ConnectedCache getConnectedCache() {
		return connected_cache;
	}
	
	public Config getCustomConfig() {
		return config;
	}
	
	public DiscordMinecraftLink getAccountLinks() {
		return account_links;
	}
	
	public DataSource getDataSource() {
		return data_source;
	}
	
	public DataSource getConnectedCacheDataSource() {
		return connected_cache_data_source;
	}
	
	public DiscordBot getBot() {
		return bot;
	}
	
	private void initializeSQLiteDatabase() {
		connected_cache_data_source =
				initializeSQLiteDatabase("connected_cache.db", "connected_cache_schema_blank.sql");
		data_source = initializeSQLiteDatabase("links.db", "schema_blank.sql");
	}
	
	private DataSource initializeSQLiteDatabase(String db_file, String schema_file) {
		getLogger().info("Initializing SQLite database " + db_file + "...");
		try {
			Class.forName("org.sqlite.JDBC");
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		SQLiteConfig config = new SQLiteConfig();
		config.setSynchronous(SQLiteConfig.SynchronousMode.FULL);
		SQLiteDataSource source = new SQLiteDataSource(config);
		Path db_path = getDataFolder().toPath().resolve(db_file);
		try {
			db_path.toFile().createNewFile();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		source.setUrl("jdbc:sqlite:" + db_path.toString());
		try {
			ensureSQLiteTables(source, schema_file);
		}
		catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		getLogger().info("SQLite database " + db_file + " initialized");
		return source;
	}
	
	private void ensureSQLiteTables(DataSource source, String schema_file) throws SQLException, IOException {
		// Load schema
		try (InputStream in = getResource(schema_file); Scanner scanner = new Scanner(in).useDelimiter("\\Z")) {
			// Read entire file
			String batch = scanner.next();
			// Split file into individual statements
			String[] raw_statements = batch.split(";");
			// Execute them all
			try (Connection connection = source.getConnection()) {
				for (String raw_statement : raw_statements) {
					if (raw_statement.trim().isEmpty()) {
						continue;
					}
					try (PreparedStatement statement = connection.prepareStatement(raw_statement.trim())) {
						statement.execute();
					}
				}
			}
		}
	}
	
	public class Config {
		public String getBotToken() {
			return getConfig().getString("bot-token");
		}
		
		public String getLinkCommand() {
			return getConfig().getString("link-command");
		}
		
		public String getUnlinkCommand() {
			return getConfig().getString("unlink-command");
		}
		
		public String getMessage(String id) {
			id = "messages." + id;
			String message = getConfig().getString(id);
			return ChatColor.translateAlternateColorCodes('&', message == null ? id : message);
		}
	}
}
