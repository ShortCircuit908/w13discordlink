package dev.shortcircuit908.w13discordlink;

import java.sql.SQLException;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.security.auth.login.LoginException;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;

public class DiscordBot {
	private final W13DiscordLink plugin;
	private JDA jda;
	
	public DiscordBot(W13DiscordLink plugin) {
		this.plugin = plugin;
	}
	
	public void start() throws LoginException, InterruptedException {
		jda = JDABuilder.create(GatewayIntent.DIRECT_MESSAGES, GatewayIntent.GUILD_MEMBERS)
				.setToken(plugin.getCustomConfig().getBotToken())
				.addEventListeners(new LinkListener())
				.build();
		jda.awaitReady();
	}
	
	public void stop() {
		if (jda != null) {
			jda.shutdown();
		}
		jda = null;
	}
	
	public Optional<String> getBotName() {
		return jda == null
				? Optional.empty()
				: Optional.of(jda.getSelfUser().getName() + "#" + jda.getSelfUser().getDiscriminator());
	}
	
	public Optional<Set<String>> getMemberRoles(String guild_id, String user_id) throws InvalidGuildException {
		if (jda == null) {
			throw new IllegalStateException("JDA is not initialized");
		}
		Guild guild = jda.getGuildById(guild_id);
		if (guild == null) {
			throw new InvalidGuildException();
		}
		Member member = guild.getMemberById(user_id);
		if (member == null) {
			return Optional.empty();
		}
		return Optional.of(member.getRoles().stream().map(Role::getName).collect(Collectors.toSet()));
	}
	
	private class LinkListener extends ListenerAdapter {
		@Override
		public void onMessageReceived(MessageReceivedEvent event) {
			if (!event.isFromType(ChannelType.PRIVATE) || event.getAuthor().getId().equals(jda.getSelfUser().getId())) {
				return;
			}
			String message = event.getMessage().getContentRaw().replaceAll("[`\\\\]", "");
			String[] args = message.trim().split("\\s");
			if (args.length < 1) {
				return;
			}
			if (args[0].equalsIgnoreCase(plugin.getCustomConfig().getLinkCommand())) {
				if (args.length < 3) {
					event.getPrivateChannel()
							.sendMessage(plugin.getCustomConfig().getMessage("bot.command-usage"))
							.complete();
					return;
				}
				String username = args[1].trim();
				String token = args[2].trim();
				try {
					Optional<UUID> target_player = plugin.getConnectedCache().getCachedUser(username);
					if (!target_player.isPresent()) {
						event.getChannel()
								.sendMessage(plugin.getCustomConfig()
										.getMessage("bot.player-not-found")
										.replace("%mc-username%", username))
								.complete();
						return;
					}
					UUID target_player_id = target_player.get();
					Optional<String> check_token = plugin.getAccountLinks().getAuthTokenFromPlayerId(target_player_id);
					if (!check_token.isPresent()) {
						event.getPrivateChannel()
								.sendMessage(plugin.getCustomConfig()
										.getMessage("bot.token-not-found")
										.replace("%mc-username%", username))
								.complete();
						return;
					}
					if (!token.equalsIgnoreCase(check_token.get())) {
						event.getPrivateChannel()
								.sendMessage(plugin.getCustomConfig().getMessage("bot.token-invalid"))
								.complete();
						return;
					}
					plugin.getAccountLinks().linkDiscordAndMinecraft(target_player_id, event.getAuthor().getId());
					plugin.getAccountLinks().clearAuthTokenForPlayerId(target_player_id);
					event.getPrivateChannel()
							.sendMessage(plugin.getCustomConfig().getMessage("bot.link-success"))
							.complete();
				}
				catch (SQLException e) {
					e.printStackTrace();
					event.getPrivateChannel()
							.sendMessage(plugin.getCustomConfig().getMessage("bot.errors.check-auth"))
							.complete();
				}
			}
			else if (args[0].equalsIgnoreCase(plugin.getCustomConfig().getUnlinkCommand())) {
				try {
					plugin.getAccountLinks().unlinkDiscordAndMinecraft(event.getAuthor().getId());
					event.getPrivateChannel()
							.sendMessage(plugin.getCustomConfig().getMessage("bot.unlink-success"))
							.complete();
				}
				catch (SQLException e) {
					e.printStackTrace();
					event.getPrivateChannel()
							.sendMessage(plugin.getCustomConfig().getMessage("bot.errors.unlink"))
							.complete();
				}
			}
		}
	}
	
	public static class InvalidGuildException extends Exception {
		public InvalidGuildException() {
			super("Guild not found");
		}
	}
}
