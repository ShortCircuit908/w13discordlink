package dev.shortcircuit908.w13discordlink;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

public class DiscordMinecraftLink {
	private final W13DiscordLink plugin;
	
	public DiscordMinecraftLink(W13DiscordLink plugin) {
		this.plugin = plugin;
	}
	
	public Optional<String> getDiscordUserFromPlayerId(UUID uuid) throws SQLException {
		ensureDataSourcePresent();
		try (Connection connection = plugin.getDataSource().getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT `discord_id` FROM `account_links` WHERE `player_id`=?")) {
			statement.setString(1, Utils.uuidToString(uuid));
			try (ResultSet result = statement.executeQuery()) {
				if (result.next()) {
					return Optional.of(result.getString(1));
				}
			}
		}
		return Optional.empty();
	}
	
	public void linkDiscordAndMinecraft(UUID player_id, String discord_id) throws SQLException {
		ensureDataSourcePresent();
		try (Connection connection = plugin.getDataSource().getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "INSERT  OR REPLACE INTO `account_links` (`player_id`, `discord_id`) VALUES (?, ?)")) {
			statement.setString(1, Utils.uuidToString(player_id));
			statement.setString(2, discord_id);
			statement.execute();
		}
	}
	
	public void unlinkDiscordAndMinecraft(UUID player_id) throws SQLException {
		ensureDataSourcePresent();
		try (Connection connection = plugin.getDataSource().getConnection();
			 PreparedStatement statement = connection.prepareStatement("DELETE FROM `account_links` WHERE " +
					 "`player_id`=?")) {
			statement.setString(1, Utils.uuidToString(player_id));
			statement.execute();
		}
	}
	
	public void unlinkDiscordAndMinecraft(String discord_id) throws SQLException {
		ensureDataSourcePresent();
		try (Connection connection = plugin.getDataSource().getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "DELETE FROM `account_links` WHERE `discord_id`=?")) {
			statement.setString(1, discord_id);
			statement.execute();
		}
	}
	
	public Optional<String> getAuthTokenFromPlayerId(UUID uuid) throws SQLException {
		ensureDataSourcePresent();
		Instant now = Instant.now();
		try (Connection connection = plugin.getDataSource().getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "SELECT `token`, `generated` FROM " + "`link_tokens` WHERE `player_id`=?")) {
			statement.setString(1, Utils.uuidToString(uuid));
			try (ResultSet result = statement.executeQuery()) {
				if (result.next()) {
					String token = result.getString(1);
					Instant timestamp = Instant.parse(result.getString(2));
					if (now.getEpochSecond() - timestamp.getEpochSecond() < (30 * 60)) {
						return Optional.of(token);
					}
				}
			}
		}
		return Optional.empty();
	}
	
	public String generateAuthTokenForPlayerId(UUID uuid) throws SQLException {
		ensureDataSourcePresent();
		Instant now = Instant.now();
		Random random = new SecureRandom();
		byte[] buffer = new byte[3];
		random.nextBytes(buffer);
		String code = Utils.bytesToHex(buffer).toLowerCase();
		try (Connection connection = plugin.getDataSource().getConnection();
			 PreparedStatement statement = connection.prepareStatement(
					 "INSERT OR REPLACE INTO `link_tokens` (`player_id`, `token`, `generated`) VALUES (?, ?, ?)")) {
			statement.setString(1, Utils.uuidToString(uuid));
			statement.setString(2, code);
			statement.setString(3, now.toString());
			statement.execute();
		}
		return code;
	}
	
	public void clearAuthTokenForPlayerId(UUID uuid) throws SQLException {
		ensureDataSourcePresent();
		try (Connection connection = plugin.getDataSource().getConnection();
			 PreparedStatement statement =
					 connection.prepareStatement("DELETE FROM `link_tokens` WHERE `player_id`=?")) {
			statement.setString(1, Utils.uuidToString(uuid));
			statement.execute();
		}
	}
	
	private void ensureDataSourcePresent() throws SQLException {
		if (plugin.getDataSource() == null) {
			throw new SQLException("Database is not initialized");
		}
	}
}
