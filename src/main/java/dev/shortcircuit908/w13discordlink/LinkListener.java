package dev.shortcircuit908.w13discordlink;

import dev.shortcircuit908.w13discordlink.event.AsyncDiscordPreLoginEvent;
import dev.shortcircuit908.w13discordlink.event.DiscordJoinEvent;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.Permissible;

public class LinkListener implements Listener {
	private final W13DiscordLink plugin;
	
	public LinkListener(W13DiscordLink plugin) {
		this.plugin = plugin;
	}
	
	private static class LoginResult {
		private boolean allowed;
		private String message;
		
		public LoginResult(boolean allowed, String message) {
			this.allowed = allowed;
			this.message = message;
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPreLogin(final AsyncPlayerPreLoginEvent event) {
		// Gather info
		final UUID player_id = event.getUniqueId();
		final String player_name = event.getName();
		plugin.getServer().getScheduler().scheduleAsyncDelayedTask(plugin, () -> {
			try {
				plugin.getConnectedCache().cacheConnectedUser(player_id, player_name, System.currentTimeMillis());
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		});
		final LoginResult result = new LoginResult(true, null);
		// If the player is a server operator, let them through
		if (!Utils.isOp(player_id)) {
			try {
				// Get Discord ID
				Optional<String> discord_user_id = plugin.getAccountLinks().getDiscordUserFromPlayerId(player_id);
				if (discord_user_id.isPresent()) {
					AsyncDiscordPreLoginEvent discord_login_event =
							new AsyncDiscordPreLoginEvent(event, discord_user_id.get());
					plugin.getServer().getPluginManager().callEvent(discord_login_event);
				}
				else {
					// The user has not linked their Discord account
					String raw_token;
					// Get the user's current auth token, if one has been generated
					Optional<String> token = plugin.getAccountLinks().getAuthTokenFromPlayerId(player_id);
					if (token.isPresent()) {
						raw_token = token.get();
					}
					else {
						// If there is not an auth token, or it has expired, generate a new token
						raw_token = plugin.getAccountLinks().generateAuthTokenForPlayerId(player_id).toUpperCase();
					}
					// Kick the player and tell them how to link accounts
					Map<String, String> replacements = new HashMap<>();
					replacements.put("%bot-name%", plugin.getBot().getBotName().get());
					replacements.put("%link-command%", plugin.getCustomConfig().getLinkCommand());
					replacements.put("%mc-username%", player_name);
					replacements.put("%auth-token%", raw_token.toUpperCase());
					String message = plugin.getCustomConfig().getMessage("not-linked");
					for (Map.Entry<String, String> replacement : replacements.entrySet()) {
						message = message.replace(replacement.getKey(), replacement.getValue());
					}
					result.allowed = false;
					result.message = message;
				}
			}
			catch (SQLException e) {
				// Oops
				e.printStackTrace();
				result.allowed = false;
				result.message = plugin.getCustomConfig().getMessage("errors.link");
			}
		}
		// Perform the kicking
		if (!result.allowed) {
			event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_WHITELIST);
			if (result.message != null) {
				event.setKickMessage(result.message);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onJoin(final PlayerJoinEvent event) {
		try {
			Optional<String> discord_user_id =
					plugin.getAccountLinks().getDiscordUserFromPlayerId(event.getPlayer().getUniqueId());
			if (!discord_user_id.isPresent()) {
				return;
			}
			DiscordJoinEvent discord_join_event = new DiscordJoinEvent(event, discord_user_id.get());
			plugin.getServer().getPluginManager().callEvent(discord_join_event);
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
