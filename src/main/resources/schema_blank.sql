CREATE TABLE IF NOT EXISTS `link_tokens` (
	`player_id` CHARACTER(32) NOT NULL,
	`token`     CHARACTER(6)  NOT NULL,
	`generated` TEXT          NOT NULL,
	UNIQUE (`player_id`),
	UNIQUE (`token`)
);

CREATE TABLE IF NOT EXISTS `account_links` (
	`player_id`  CHARACTER(32) NOT NULL,
	`discord_id` CHARACTER(20) NOT NULL,
	UNIQUE (`player_id`, `discord_id`)
);
