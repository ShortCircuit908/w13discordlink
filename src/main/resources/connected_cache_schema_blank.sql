CREATE TABLE IF NOT EXISTS `connected_cache` (
	`player_id`   CHARACTER(32) NOT NULL,
	`username`    TEXT          NOT NULL,
	`last_joined` INTEGER       NOT NULL,
	UNIQUE (`player_id`)
);

